package channel

import (
	"errors"
	"go-unittest/stage1/db"

	"github.com/gofiber/fiber/v2"
)

type Channel struct {
}

func New() *Channel {
	return &Channel{}
}

// DELETE /channel/{id}
func (c *Channel) Delete(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	database := db.New()
	posts := database.GetAllPostsByChannelId(id)

	if len(posts) > 10 {
		return errors.New("this is a dangerous operation because this channel has many posts. Please contact with SafeChat to solve")
	}

	channel := database.GetChanelById(id)
	if channel == nil {
		return errors.New("Channel is not existed to delete")
	}
	database.RemoveChannel(id)

	for _, p := range posts {
		database.RemovePost(p)
	}

	return nil
}
