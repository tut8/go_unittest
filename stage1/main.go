package main

import (
	"go-unittest/stage1/channel"
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	channel := channel.New()

	app := fiber.New()
	app.Delete("/api/channel", channel.Delete)

	log.Fatal(app.Listen(":1234"))
}
