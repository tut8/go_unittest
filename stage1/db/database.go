package db

import (
	"go-unittest/model"
)

type Database struct {
	channels map[string]*model.Channel
	posts    map[string]*model.Post
}

func New() *Database {
	return &Database{
		channels: make(map[string]*model.Channel),
		posts:    make(map[string]*model.Post),
	}
}

func (d *Database) RemoveChannel(id string) {
	delete(d.channels, id)
}

func (d *Database) RemovePost(id string) {
	delete(d.channels, id)
}

func (d *Database) GetChanelById(id string) *model.Channel {
	return d.channels[id]
}

func (d *Database) GetAllPostsByChannelId(id string) []string {
	rs := make([]string, 0)

	for _, v := range d.posts {
		if v.ChannelId == id {
			rs = append(rs, v.Id)
		}
	}

	return rs
}
