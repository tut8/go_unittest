package handler

import (
	"errors"
	"go-unittest/stage2/channel"

	"github.com/gofiber/fiber/v2"
)

type Handler struct {
}

func New() *Handler {
	return &Handler{}
}

// DELETE /channel/{id}
func (h *Handler) DeleteChannel(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	if id == "" {
		return errors.New("id is invalid")
	}

	channel := channel.New()
	return channel.Delete(id)
}
