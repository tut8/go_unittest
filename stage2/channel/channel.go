package channel

import (
	"errors"
	"go-unittest/stage1/db"
)

type Channel struct {
}

func New() *Channel {
	return &Channel{}
}

func (c *Channel) Delete(id string) error {
	database := db.New()
	posts := database.GetAllPostsByChannelId(id)

	if len(posts) > 10 {
		return errors.New("this is a dangerous operation because this channel has many posts. Please contact with SafeChat to solve")
	}

	channel := database.GetChanelById(id)
	if channel == nil {
		return errors.New("Channel is not existed to delete")
	}
	database.RemoveChannel(id)

	for _, p := range posts {
		database.RemovePost(p)
	}

	return nil
}
