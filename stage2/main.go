package main

import (
	"go-unittest/stage2/handler"
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	handler := handler.New()

	app := fiber.New()
	app.Delete("/api/channel", handler.DeleteChannel)

	log.Fatal(app.Listen(":1234"))
}
