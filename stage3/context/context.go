package context

import "github.com/gofiber/fiber/v2"

type Context interface {
	GetParam(name string) string
}

type fiberContext struct {
	ctx *fiber.Ctx
}

func NewContext(ctx *fiber.Ctx) *fiberContext {
	return &fiberContext{
		ctx: ctx,
	}
}

func (c *fiberContext) GetParam(name string) string {
	return c.ctx.Params(name)
}
