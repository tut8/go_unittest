package main

import (
	"go-unittest/stage3/channel"
	"go-unittest/stage3/context"
	"go-unittest/stage3/db"
	"go-unittest/stage3/handler"
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	db := db.New()
	channel := channel.New(db)
	h := handler.New(channel)

	app := fiber.New()
	app.Delete("/api/channel", func(c *fiber.Ctx) error {
		return h.DeleteChannel(context.NewContext(c))
	})

	log.Fatal(app.Listen(":1234"))
}
