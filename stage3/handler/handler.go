package handler

import (
	"errors"
	"go-unittest/stage3/channel"
	"go-unittest/stage3/context"
)

type handler struct {
	channel channel.Channel
}

func New(channel channel.Channel) *handler {
	return &handler{
		channel: channel,
	}
}

// DELETE /channel/{id}
func (h *handler) DeleteChannel(ctx context.Context) error {
	id := ctx.GetParam("id")
	if id == "" {
		return errors.New("id is invalid")
	}

	return h.channel.Delete(id)
}
