package handler

import (
	"go-unittest/stage3/context"
	"testing"

	"github.com/stretchr/testify/mock"
)

type channelMock struct {
	mock.Mock
}

func (c *channelMock) Delete(id string) error {
	return c.Called(id).Error(0)

}

type ctxMock struct {
	mock.Mock
}

func (c *ctxMock) GetParam(name string) string {
	return c.Called(name).String(0)
}

func Test_handler_DeleteChannel(t *testing.T) {
	tests := []struct {
		name    string
		sut     func() *handler
		arg     func() context.Context
		wantErr bool
	}{
		{
			name: "Test sample",
			sut: func() *handler {
				c := &channelMock{}
				c.On("Delete", "test").Return(nil)

				return &handler{
					channel: c,
				}
			},
			arg: func() context.Context {
				ctx := &ctxMock{}
				ctx.On("GetParam", "id").Return("test")

				return ctx
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.sut().DeleteChannel(tt.arg()); (err != nil) != tt.wantErr {
				t.Errorf("handler.DeleteChannel() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
