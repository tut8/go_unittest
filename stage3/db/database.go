package db

import (
	"go-unittest/model"
)

type Database interface {
	RemoveChannel(id string)
	RemovePosts(ids []string)
	GetChanelById(id string) *model.Channel
	GetAllPostsByChannelId(id string) []string
}

type memoryDb struct {
	channels map[string]*model.Channel
	posts    map[string]*model.Post
}

func New() *memoryDb {
	return &memoryDb{
		channels: make(map[string]*model.Channel),
		posts:    make(map[string]*model.Post),
	}
}

func (d *memoryDb) RemoveChannel(id string) {
	delete(d.channels, id)
}

func (d *memoryDb) RemovePosts(ids []string) {
	for _, id := range ids {
		delete(d.channels, id)
	}
}

func (d *memoryDb) GetChanelById(id string) *model.Channel {
	return d.channels[id]
}

func (d *memoryDb) GetAllPostsByChannelId(id string) []string {
	rs := make([]string, 0)

	for _, v := range d.posts {
		if v.ChannelId == id {
			rs = append(rs, v.Id)
		}
	}

	return rs
}
