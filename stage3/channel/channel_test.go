package channel

import (
	"fmt"
	"go-unittest/model"
	"go-unittest/stage3/db"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type dbMock struct {
	mock.Mock
}

func (d *dbMock) RemoveChannel(id string) {
	d.Called(id)
}

func (d *dbMock) RemovePosts(ids []string) {
	d.Called(ids)
}

func (d *dbMock) GetChanelById(id string) *model.Channel {
	r := d.Called(id).Get(0)
	if r != nil {
		return r.(*model.Channel)
	}
	return nil
}

func (d *dbMock) GetAllPostsByChannelId(id string) []string {
	r := d.Called(id).Get(0)
	fmt.Printf("GetAllPostsByChannelId: %v", r)
	return r.([]string)
}

func Test_channel_Delete(t *testing.T) {
	tests := []struct {
		name    string
		sut     func() *channel
		args    string
		wantErr bool
	}{
		{
			name: "will delete channel and all related posts",
			sut: func() *channel {
				channelId := "channel_id"
				postsIds := []string{"1", "2"}

				db := &dbMock{}
				db.On("GetAllPostsByChannelId", channelId).Return(postsIds)
				db.On("GetChanelById", channelId).Return(&model.Channel{
					Id: channelId,
				})
				db.On("RemoveChannel", channelId)
				db.On("RemovePosts", postsIds)

				return &channel{database: db}
			},
			args:    "channel_id",
			wantErr: false,
		},
		{
			name: "throws when post is greater 2",
			sut: func() *channel {
				channelId := "channel_id"
				postsIds := []string{"1", "2", "3"}

				db := &dbMock{}
				db.On("GetAllPostsByChannelId", channelId).Return(postsIds)

				return &channel{database: db}
			},
			args:    "channel_id",
			wantErr: true,
		},
		{
			name: "throws when channel is not exists",
			sut: func() *channel {
				channelId := "channel_id"
				postsIds := []string{"1", "2"}

				db := &dbMock{}
				db.On("GetAllPostsByChannelId", channelId).Return(postsIds)
				db.On("GetChanelById", channelId).Return(nil)

				return &channel{database: db}
			},
			args:    "channel_id",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.sut().Delete(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("channel.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNew(t *testing.T) {
	tests := []struct {
		name string
		args db.Database
		want func(*channel) bool
	}{
		{
			name: "sub will create channel service",
			args: &dbMock{},
			want: func(c *channel) bool {
				return assert.NotNil(t, c) && assert.IsType(t, &dbMock{}, c.database)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(tt.args); !tt.want(got) {
				t.Errorf("New() = %v", got)
			}
		})
	}
}
