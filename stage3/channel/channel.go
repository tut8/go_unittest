package channel

import (
	"errors"
	"go-unittest/stage3/db"
)

type Channel interface {
	Delete(id string) error
}

type channel struct {
	database db.Database
}

func New(database db.Database) *channel {
	return &channel{
		database: database,
	}
}

func (c *channel) Delete(id string) error {
	posts := c.database.GetAllPostsByChannelId(id)

	if len(posts) > 2 {
		return errors.New("this is a dangerous operation because this channel has many posts. Please contact with SafeChat to solve")
	}

	channel := c.database.GetChanelById(id)
	if channel == nil {
		return errors.New("Channel is not existed to delete")
	}
	c.database.RemoveChannel(id)
	// c.database.RemovePosts(posts)

	return nil
}
