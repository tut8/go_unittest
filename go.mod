module go-unittest

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/stretchr/testify v1.7.0
)
