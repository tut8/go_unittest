package model

type Channel struct {
	Id    string
	Name  string
	Posts []*Post
}

type Post struct {
	Id        string
	ChannelId string
	Name      string
}
